# https://github.com/digidotcom/python-xbee/blob/master/doc/user_doc/discovering_the_xbee_network.rst


from digi.xbee.devices import XBeeDevice
import time

def main():
    print(" +---------------------------------------------------------------------------+")
    print(" | XBee Python Library: Find 1 device with NI node identifier and send data  |")
    print(" +---------------------------------------------------------------------------+\n")

    device = XBeeDevice("/dev/tty.usbserial-DN040BLV", 9600)
    device.open()
    
    # Get the XBee Network object from the XBee device.
    xbee_network = device.get_network()

    #XCTU: NI node identifier: REMOTE (be careful: space before name
    remote_device = xbee_network.discover_device("R1")   
    print ("remote_device", remote_device)
    device.send_data(remote_device, "Hello worldSSSSSSSSS")


    device.close()


if __name__ == '__main__':
    main()
