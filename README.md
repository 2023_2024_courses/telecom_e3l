# telecom_e3l


```bash
├── crypt_analysis
│   ├── ch7.bin
│   ├── ch7.bin.md
│   ├── en_dico.txt
│   ├── en_ngram.csv
│   ├── fr_dico.txt
│   ├── fr_ngram.csv
│   └── histogram.py
├── crypto
│   ├── aes
│   │   ├── aes-128-cbc.c
│   │   ├── aes-128-cbc_base.c
│   │   ├── aes_cbc.py
│   │   ├── aes_ctr.py
│   │   └── aes_ecb.py
│   ├── hash
│   │   └── sha256.py
│   └── rsa
│       ├── rsa_compute_key_2.py
│       ├── rsa_compute_key_3_PEM.py
│       └── rsa_compute_key.py
├── docker
│   ├── docker-compose.yml
│   └── Dockerfile
├── README.md
├── socket
│   ├── client_ipv4_ipv6.c
│   └── server_ipv4_ipv6.c
└── zigbee
    ├── 1_broadcast.py
    ├── 2_FindAllDevicesSendData.py
    └── 3_Find_NI_SendData.py    
```
