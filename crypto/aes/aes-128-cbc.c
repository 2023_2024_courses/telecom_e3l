//# gcc -o aes-128-cbc aes-128-cbc.c -lcrypto

	
#include <openssl/blowfish.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <string.h>


#define IP_SIZE 1024
#define OP_SIZE 1060  // Minimum (sizeof (buffer in) + cipher_block_size - 1)

static int crypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV);
static int decrypt (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV);



//unsigned char key[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//unsigned char iv [] = {1,2,3,4,5,6,7,8};

//openssl enc bf-cbc -e -in clearFile.txt -out cryptFile1  -nosalt 
//                          -K 01000000000000000000000000000000 -iv 0100000000000000
// or
//openssl enc -aes-128-cbc -e -in t.txt -out probleme4  -nosalt -K 01 -iv 01 -P


unsigned char key[] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
unsigned char iv [] = {3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

int main (int argc, char *argv[])
{

    crypt   ("clearFile.txt", "cryptFile",       &key[0], &iv[0]);
    decrypt ("cryptFile",     "decryptFile.txt", &key[0], &iv[0]);
    return (0);
}


/*
EVP_EncryptUpdate() encrypts inl bytes from the buffer in and writes the encrypted version to out. This function can be called multiple times to encrypt successive blocks of data. The amount of data written depends on the block alignment of the encrypted data: as a result the amount of data written may be anything from zero bytes to (inl + cipher_block_size - 1) so outl should contain sufficient room. The actual number of bytes written is placed in outl.
*/



static int crypt   (char *pClearFileName, char *pCryptFileName, unsigned char *pKey, unsigned char *pIV)
{
FILE          *pClearFile;
FILE          *pCryptFile;
unsigned char  outbuf[OP_SIZE];
unsigned char  inbuff[IP_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding=0;

EVP_CIPHER_CTX *ctx;

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_EncryptInit (ctx, EVP_aes_128_cbc (), pKey, pIV);

    pClearFile = fopen(pClearFileName, "rb");
    pCryptFile = fopen(pCryptFileName, "wb");

    memset (&inbuff[0], 0, sizeof(inbuff));
    memset (&outbuf[0], 0, sizeof(outbuf));

    for (; (nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pClearFile));)
    {


        if (EVP_EncryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead) != 1)
        {
            printf ("error in encrypt update\n");
            return 0;
        }
        fwrite (&outbuf[0], 1, lenCrypt, pCryptFile);

    }

    if (EVP_EncryptFinal (ctx, &outbuf[lenCrypt], &lenPadding) != 1)
    {
        printf ("error in encrypt final\n");
        return 0;
    }
    else
    {
        fwrite (&outbuf[lenCrypt], 1, lenPadding, pCryptFile);
    }

    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose (pClearFile);
    fclose (pCryptFile);
    return (0);
}


static int decrypt   (char *pCryptFileName, char *pDecryptFileName, unsigned char *pKey, unsigned char *pIV)
{
FILE          *pCryptFile;
FILE          *pDecryptFile;
unsigned char  outbuf[OP_SIZE];
unsigned char  inbuff[IP_SIZE];
int            nbByteRead;
int            lenCrypt  =0;
int            lenPadding=0;

EVP_CIPHER_CTX *ctx;

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init (ctx);
    EVP_DecryptInit (ctx, EVP_aes_128_cbc (), pKey, pIV);




    pCryptFile   = fopen(pCryptFileName,   "rb");
    pDecryptFile = fopen(pDecryptFileName, "wb");
    memset (&inbuff, 0, sizeof(inbuff));
    memset (&outbuf, 0, sizeof(outbuf));

    for (; (nbByteRead=fread(&inbuff[0], 1, sizeof(inbuff), pCryptFile));)
    {

        if (EVP_DecryptUpdate (ctx, outbuf, &lenCrypt, inbuff, nbByteRead) != 1)
        {
            printf ("error in decrypt update\n");
            return 0;
        }
        fwrite (&outbuf[0], 1, lenCrypt, pDecryptFile);

    }

    if (EVP_DecryptFinal (ctx, &outbuf[lenCrypt], &lenPadding) != 1)
    {
        printf ("error in decrypt final\n");
        return 0;
    }
    fwrite (&outbuf[lenCrypt], 1, lenPadding, pDecryptFile);

    EVP_CIPHER_CTX_cleanup (ctx);
    EVP_CIPHER_CTX_free(ctx);
    fclose (pDecryptFile);
    fclose (pCryptFile);
    return (0);
}



