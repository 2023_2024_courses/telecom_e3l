####################

# pip install pyasn1


#####################

from pyasn1.type import univ, namedtype
from pyasn1.codec.der import encoder
import base64



def save_pkcs1_pem(n, d, e, p, q, exp1, exp2, coef):
    '''
    Saves the private key in PKCS#1 DER format.
    '''

    class AsnPrivKey(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('version', univ.Integer()),
            namedtype.NamedType('modulus', univ.Integer()),
            namedtype.NamedType('publicExponent', univ.Integer()),
            namedtype.NamedType('privateExponent', univ.Integer()),
            namedtype.NamedType('prime1', univ.Integer()),
            namedtype.NamedType('prime2', univ.Integer()),
            namedtype.NamedType('exponent1', univ.Integer()),
            namedtype.NamedType('exponent2', univ.Integer()),
            namedtype.NamedType('coefficient', univ.Integer()),
        )

    # Create the ASN object
    asn_key = AsnPrivKey()
    asn_key.setComponentByName('version', 0)
    asn_key.setComponentByName('modulus', n)
    asn_key.setComponentByName('publicExponent', e)
    asn_key.setComponentByName('privateExponent', d)
    asn_key.setComponentByName('prime1', p)
    asn_key.setComponentByName('prime2', q)
    asn_key.setComponentByName('exponent1', exp1)
    asn_key.setComponentByName('exponent2', exp2)
    asn_key.setComponentByName('coefficient', coef)

#    b64 = base64.encodestring(encoder.encode(asn_key))
    b64 = base64.encodebytes(encoder.encode(asn_key))

    asn_key_pem = "-----BEGIN RSA PRIVATE KEY-----\n%s-----END RSA PRIVATE KEY-----" % b64

    return asn_key_pem


def egcd(a, b):
    if b == 0:
        return a, 1, 0
    else:
        g, x, y = egcd(b, a % b)
        return (g, y, x - (a // b) * y)

p=17554280215883783039
q=16502540569370788733


n=p*q
phi = (p-1)*(q-1)
e=0x10001

g,x,y=egcd(phi, e)
if g == 1:
    d=y%phi
    pem=save_pkcs1_pem(n, d, e, p, q, d%(p-1), d%(q-1), 0)
    print pem

else:
    print "e and phi are not relatively prime"
    print "e=", e, "phi=", phi




