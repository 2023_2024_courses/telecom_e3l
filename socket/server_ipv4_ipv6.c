// gcc -Wall -o server_ipv4_ipv6 server_ipv4_ipv6.c

#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "string.h"
#include "stdio.h"
#include "unistd.h"
#include "signal.h"
#include <pthread.h>

static int socketIPv4(void);
static int socketIPv6(void);

int main()
{
    socketIPv4();
    //socketIPv6();

    return 0;
}


static int socketIPv4(void)
{
int	            s;
struct sockaddr_in  from;
struct sockaddr_in  sin;
socklen_t           alen;
char                buffer1[32];
char                buffer2[32];

    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family      = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(1234);

    alen = bind (s, (struct sockaddr*) &sin, sizeof (sin));

    for (;;)
    {
        alen = sizeof (from);
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        strcpy (&buffer2[0], "Good service");
        recvfrom (s, &buffer1[0], sizeof(buffer1), 0, (struct sockaddr*) &from, &alen);
        printf ("server: %s\n", &buffer1[0]);
        sendto (s, &buffer2[0], strlen (&buffer2[0]), 0, (struct sockaddr*)&from, alen); 
    }
    close (s);
    return 0;
}




static int socketIPv6(void)
{
int	            s;
struct sockaddr_in6 from;
struct sockaddr_in6 sin;
socklen_t           alen;
char                buffer1[32];
char                buffer2[32];


    s = socket (AF_INET6, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));

    sin.sin6_family = AF_INET6;
    sin.sin6_addr   = in6addr_any;
    sin.sin6_port   = htons(1234);
    alen            = bind (s, (struct sockaddr*) &sin, sizeof (sin));

    for (;;)
    {
        alen = sizeof (from);
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        strcpy (&buffer2[0], "Good service");
        recvfrom (s, &buffer1[0], sizeof(buffer1), 0, (struct sockaddr*) &from, &alen);
        printf ("server: %s\n", &buffer1[0]);
        sendto (s, &buffer2[0], strlen (&buffer2[0]), 0, (struct sockaddr*)&from, alen); 
    }
    close (s);
    return 0;
}




