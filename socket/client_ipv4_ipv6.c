// gcc -Wall -o client_ipv4_ipv6 client_ipv4_ipv6.c

#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "string.h"
#include "stdio.h"
#include "unistd.h"
#include "stdlib.h" 

#define NB_LOOP     3
static int socketIPv4(void);
static int socketIPv6(void);

struct TEST
{
   char   buffer[12];
   char   val1;
   int    val2;
};

int main()
{

    socketIPv4();
    //socketIPv6();
    return 0;
}


static int socketIPv4(void)
{
struct sockaddr_in  sin;
int		    s;
char		    buffer1[32];
char                buffer2[32];
//int                 i;
struct TEST         val;
    


    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    //inet_pton(AF_INET, "192.168.0.11", &sin.sin_addr);
    sin.sin_port = htons(1234);
    connect (s, (struct sockaddr *) &sin, sizeof(sin));


    memset (&val, 0x0, sizeof(val));
    memset (&buffer1[0], 0x0, sizeof(buffer1));
    memset (&buffer2[0], 0x0, sizeof(buffer2));
    //printf ("Enter text: \n");
    //scanf ("%s", &buffer1[0]);
    val.val1=1;
    val.val2=htonl(0x11223344);
    //write (s, &buffer1[0], sizeof(buffer1));
    write (s, &val, sizeof(val));
    read  (s, &buffer2[0], sizeof(buffer2));
    printf ("client: %s\n", &buffer2[0]);


    close (s);
    printf("stop\n");

    return 0;
}



static int socketIPv6(void)
{
struct sockaddr_in6 sin;
int		    s;
char		    buffer1[32];
char                buffer2[32];
int                 i;
    
	
    
    s = socket (AF_INET6, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin6_family = AF_INET6;
    sin.sin6_port   = htons(1234);
    inet_pton(AF_INET6, "::1", &sin.sin6_addr);
    connect (s, (struct sockaddr *) &sin, sizeof(sin));
    

    printf("start\n");
    
    for (i=0; i<NB_LOOP; i++)
    {
        memset (&buffer1[0], 0, sizeof(buffer1));
        memset (&buffer2[0], 0, sizeof(buffer2));
        printf ("Enter text: \n");
        scanf ("%s", &buffer1[0]);
        write (s, &buffer1[0], sizeof(buffer1));
        read  (s, &buffer2[0], sizeof(buffer2));
        printf ("client: %s\n", &buffer2[0]);
    }
    close (s);
    printf("stop\n");

    return 0;
}

